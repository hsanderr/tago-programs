#Most of the code was taken from reference 1

import paho.mqtt.client as mqtt
import sys
import random
import time
import json

#Definitions
#put here your device token
device_token = 'bc9ff2f2-e8f0-4435-8772-44fda73763d5' 
    
broker = "mqtt.tago.io"
broker_port = 1883
mqtt_keep_alive = 60
    
#MQTT publish topic must be tago/data/post
mqtt_publish_topic = "tago/data/post" 

#put any name here, TagoIO doesn't validate this username.    
mqtt_username = 'mqtt_client' 

#MQTT password must be the device token (TagoIO does validate this password)
mqtt_password = device_token 

#Callback - MQTT broker connection is on
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        client.connected_flag = True #Set connection flag to True (connected)
        print("[STATUS] Connected to MQTT broker. Result: " + str(rc))
    else:
        print("[STATUS] Bad connection. Returned code: " + str(rc))

#Generate a random temperature to send to TagoIO (from 32 to 86F)
temperature = random.randint(32,86)
txt_temperature="Random temperature generated: {rand_temp}F"
print(txt_temperature.format(rand_temp=temperature) )

#Format data into JSON
temperature_json = {"variable": "temperature", "unit": "F", "value": temperature}
temperature_json_string = json.dumps(temperature_json)

#Main program
print("[STATUS] Initializing MQTT...")
mqtt.Client.connected_flag = False #Create connection flag and set to False
				                   #False => disconnected, True => connected
				                   #See reference 2
client = mqtt.Client(protocol=mqtt.MQTTv311)
client.username_pw_set(mqtt_username, mqtt_password)
client.on_connect = on_connect
client.loop_start()
client.connect(broker, broker_port, mqtt_keep_alive)

#Send data to TagoIO using MQTT
while not client.connected_flag:
	print("Waiting connection")
	time.sleep(1)
client.publish(mqtt_publish_topic, temperature_json_string)
print(temperature_json_string)
print("Data sent to TagoIO platform")
client.loop_stop()
client.disconnect()

#Reference 1: https://help.tago.io/portal/en/community/topic/raspberry-pi-tagoio-using-sdk-or-mqtt-getting-started
#Reference 2: http://www.steves-internet-guide.com/client-connections-python-mqtt/
